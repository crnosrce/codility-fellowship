
def move_left(string, curr_index, final_pos):
    tail = string[curr_index + 1:] if curr_index < (len(string) - 1) else ""
    result = string[0:final_pos] + string[curr_index] + string[final_pos:curr_index] + tail
    return result


def search_back_while_smaller(min_search_string, char, curr_index, desired_final_pos):
    final_pos = curr_index
    while final_pos > desired_final_pos and min_search_string[final_pos - 1] > char:
        final_pos -= 1
    return final_pos


def move_left_while_smaller(curr_s_min, curr_index, desired_final_pos):
    final_pos = curr_index
    if curr_index > 0:
        char = curr_s_min[curr_index]
        for idx in range(curr_index - 1, desired_final_pos - 1, -1):
            if char >= curr_s_min[idx]:
                return final_pos
            final_pos -= 1
    return final_pos


def remove_index(list_to_edit, index_to_remove):
    tail = list_to_edit[index_to_remove + 1:] if index_to_remove < (len(list_to_edit) - 1) else []
    edited_list = list_to_edit[0:index_to_remove] + tail
    return edited_list


def remove_next_best(min_search_string, min_index, curr_s_min, curr_k):
    # since position trumps direct value in a move, the best
    # moves will be those that put the lowest value char as
    # far to the left as possible. In a string like:
    # ...j..a
    # where curr_s_min = [a, j, ...]
    # the end result of moving a by 1 will be much higher than moving
    # j to the left by 1, so what we need to do is search through the
    # curr_s_min list to find which one can be moved nearest to the left.
    # Once found it can be eliminated from the curr_s_min and the process
    # can be repeated for the next round
    leftmost = len(min_search_string) - 1
    best_index = leftmost
    final_pos = leftmost
    s_min_index = len(curr_s_min) - 1
    for idx, char in enumerate(curr_s_min):
        curr_index = min_search_string.index(char, min_index)
        if curr_index == min_index:
            # one of these is already in the best position so keep it
            return curr_index, curr_index, remove_index(curr_s_min, idx)
        desired_final_pos = max(min_index, curr_index - curr_k)
        final_pos = move_left_while_smaller(min_search_string, curr_index, desired_final_pos)
        if final_pos != curr_index and final_pos < leftmost:
            leftmost = final_pos
            best_index = curr_index
            s_min_index = idx
        if leftmost == min_index:
            break  # shortcut - we can't do better
    next_s_min = remove_index(curr_s_min, s_min_index)
    return best_index, leftmost, next_s_min


def compare_to_sorted(S, K):
    N = len(S)
    S_min = sorted(S)
    min_string = S
    min_search_string = list(S)  # used for recording which letters have already been moved
    min_index = 0
    final_pos = N - 1
    curr_k = K
    curr_s_min = list(S_min)
    while curr_k > 0 and len(curr_s_min) > 0:
        # s_min_char = S_min[min_index]
        # curr_index = min_search_string.index(s_min_char)
        curr_index, final_pos, curr_s_min = remove_next_best(min_search_string, min_index, curr_s_min, curr_k)
        # move the current min char as far as possible
        # to reach its minimum position in S_min
        dist_to_move = curr_index - final_pos  # can't move past zero!
        min_string = move_left(min_string, curr_index, final_pos)
        min_search_string[curr_index] = ' '  # blank space now that the char has been moved
        min_search_string = list(move_left(''.join(min_search_string), curr_index, final_pos))  # replicate the move to the result string
        curr_k -= dist_to_move
        min_index = final_pos + 1  # nothing will move past this position now
    return min_string


def solution(S, K):
    return compare_to_sorted(S, K)
