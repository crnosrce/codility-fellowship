import random
import cProfile
import pstats

from common import generate_large_random, valid_chars, MAX_STRING_LEN, MAX_K
from map_letters import map_letters
from itertools import product


def run_hard(test_data, function_to_profile):
    for idx, test in enumerate(test_data):
        print("Test run {}: N = {}, K = {}".format(idx, len(test[0]), test[1]))
        for _ in range(10):
            function_to_profile(test[0], test[1])


def profile(words, swap_limits, function_to_profile):
    print('Num words = {}, word lengths = {}, swap_limits = {}'.format(len(words), [len(x) for x in words], swap_limits))
    test_data = list(product(words, swap_limits))
    print("Number of test products to run = {}".format(len(test_data)))
    pr = cProfile.Profile()
    pr.enable()
    run_hard(test_data, function_to_profile)
    pr.disable()
    pstats.Stats(pr).sort_stats('time', 'cumulative').print_stats()


def main():
    random.seed(129)  # so random
    functions_to_profile = [map_letters]
    for function_to_profile in functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing large set of randomly sized words:')
        approx_N = 100000
        #swap_limits = [MAX_K, (approx_N * approx_N) // 100, 90000, 1000, 100, 10, 1]
        #swap_limits = [90000, 1000]
        swap_limits = [90000]
        # words = generate_large_random(char_set=valid_chars, num_strings=2, total_len=2 * approx_N)
        words = []
        extra_words = ['e' * (approx_N - 10) + 'jihgfedcba', 'e' * (approx_N - 1000) + generate_large_random(char_set=valid_chars, num_strings=1, total_len=1000)[0]]
        words.extend(extra_words)
        profile(words, swap_limits, function_to_profile)

        # print('Testing max sized set of single letter words:')
        # words = generate_large_random(char_set=valid_chars, num_strings=MAX_NUM_STRINGS, total_len=MAX_TOTAL_LEN)
        # profile(words, function_to_profile)


if __name__ == '__main__':
    main()
