import unittest

import random

from common import MAX_K
from compare_to_sorted import move_left
from map_letters import map_letters

solvers = [map_letters]


class FellowshipTestCase(unittest.TestCase):

    def setUp(self):
        random.seed(129)  # so random

    def _test_given_cases_for(self, solver):
        self.assertEqual("adcede", solver("decade", 4))
        self.assertEqual("babbbbb", solver("bbbabbb", 2))
        self.assertEqual("aaaaabrcdbr", solver("abracadabra", 15))

    def test_given_cases_for(self):
        for solver in solvers:
            self._test_given_cases_for(solver)

    def _test_tiny(self, solver):
        self.assertEqual("", solver("", 10))
        self.assertEqual("a", solver("a", 1))
        self.assertEqual("a", solver("a", 10))
        self.assertEqual("abc", solver("cba", 10))
        self.assertEqual("abc", solver("cba", 3))

    def test_tiny(self):
        for solver in solvers:
            self._test_tiny(solver)

    def _test_more_given(self, solver):
        self.assertEqual("xztztyx", solver("zxzttyx", 2))
        self.assertEqual("yqctqfgxwmdfplglyamebrepitjadv", solver("yqctqfgxwmdfplglyamebrepitjadv", 0))
        self.assertEqual("fjbkyczfwspeiwlpzgpafpggktxjlwu", solver("fjkbyczfwspeiwlpzgpafpggktxjlwu", 1))
        self.assertEqual("efvgmtsphmhokymnxsxzsgapwjnjmwhjt", solver("vfegmtsphmhokymnxsxzsgapwjnjmwhjt", 3))
        self.assertEqual("pqrstqrstqrstqrstlmnolmnolmnolmno", solver("qrstqrstqrstqrstplmnolmnolmnolmno", 16))
        self.assertEqual("jyyuppppnnlifffbbbaa", solver("yyuppppnnljifffbbbaa", 10))

    def test_more_given(self):
        for solver in solvers:
            self._test_more_given(solver)

    def _test_huge_k(self, solver):
        self.assertEqual("", solver("", MAX_K))
        self.assertEqual("a", solver("a", MAX_K))
        self.assertEqual("abc", solver("cba", MAX_K))
        self.assertEqual("abcdef", solver("fedcba", MAX_K))
        input_string = "qrstqrstqrstqrstplmnolmnolmnolmno"
        expected_string = ''.join(sorted(list(input_string)))
        self.assertEqual(expected_string, solver(input_string, MAX_K))
        self.assertEqual("lpqrstqrstqrstqrstmnolmnolmnolmno", solver(input_string, len(input_string)))

    def test_huge_k(self):
        for solver in solvers:
            self._test_huge_k(solver)

    def _test_tiny_k(self, solver):
        self.assertEqual("", solver("", 0))
        self.assertEqual("a", solver("a", 0))
        self.assertEqual("cba", solver("cba", 0))

    def test_tiny_k(self):
        for solver in solvers:
            self._test_tiny_k(solver)

    def _test_medium(self, solver):
        self.assertEqual("abcghiklnoopstuuuvyz", solver('abcghiklnoopstuuuvyz', 400))

    def test_medium(self):
        for solver in solvers:
            self._test_medium(solver)

    def test_move_left(self):
        self.assertEqual("cabde", move_left("abcde", 2, 0))
        self.assertEqual("acbde", move_left("abcde", 2, 1))
        self.assertEqual("abecd", move_left("abcde", 4, 2))
        self.assertEqual("eabcd", move_left("abcde", 4, 0))
        self.assertEqual("abcde", move_left("abcde", 0, 0))
        self.assertEqual("bacde", move_left("abcde", 1, 0))
        self.assertEqual("a", move_left("a", 0, 0))


if __name__ == '__main__':
    unittest.main()
